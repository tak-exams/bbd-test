# -*- coding: utf-8 -*-
{
    'name': 'Project Report',
    'version': '0.1',
    'author': 'Tariq',
    'website': '',
    'category': 'Project Management',
    'summary': 'Project Report',
    'depends': [
        'project', 'report'
    ],
    'description': """
    """,
    'data': [
        'report/report_project_task.xml',
        'views/project_report_views.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
