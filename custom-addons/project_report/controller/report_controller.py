# -*- coding: utf-8 -*-

from openerp import http
from openerp.http import request


class ProjectReportController(http.Controller):
    @http.route('/api/report/task', type='json', auth="none")
    def task_data(self):
        project_task_obj = request.env['project.task'].sudo()
        all_task_records = project_task_obj.search([('project_id', '!=', False)])

        data = {}
        request.env['project.task.summary'].process_report_data(all_task_records, data)
        return data
