# -*- coding: utf-8 -*-

from openerp import models, api, fields
from datetime import datetime, date
from dateutil.relativedelta import relativedelta


class ProjectTaskSummary(models.AbstractModel):
    _name = 'project.task.summary'

    @api.multi
    def summary_report(self):
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'project_report.report_project_task',
            'datas': None,
            'report_type': "qweb-html",
        }

    @staticmethod
    def process_report_data(all_task_records, data):
        def _get_date_time(date_time, datetime_format="%Y-%m-%d"):
            """Convert date string to object
            :param date_time: string
            :param datetime_format: string
            @:return: datetime
            """
            if not isinstance(date_time, (date, datetime)):
                return datetime.strptime(date_time, datetime_format)
            return date_time

        def _get_specific_datetime(start_date, days=0, months=0, years=0, hours=0, minutes=0, seconds=0, date_format="%Y-%m-%d"):
            """Construct a new datetime (future or past)
            @:return datetime
            """
            start_date = _get_date_time(date_time=start_date, datetime_format=date_format)
            return start_date + relativedelta(days=days, months=months, years=years, hours=hours, minutes=minutes, seconds=seconds)

        today = _get_date_time(fields.Date.today())
        month_start = _get_specific_datetime(start_date=today, days=-today.day + 1)
        week_start = _get_specific_datetime(start_date=today, days=-today.weekday())
        today_last_time = _get_specific_datetime(start_date=today, hours=+23, minutes=+59, seconds=+59)

        def _check_month_week(current_task, dict_data):
            if current_task.date_end:
                current_task_end_time = _get_date_time(date_time=current_task.date_end, datetime_format="%Y-%m-%d %H:%M:%S")
                if month_start <= current_task_end_time <= today_last_time:
                    dict_data['finish_this_month'] = dict_data['finish_this_month'] + 1
                if week_start <= current_task_end_time <= today_last_time:
                    dict_data['finish_this_week'] = dict_data['finish_this_week'] + 1

        for task in all_task_records:
            if not data.get(task.user_id.id, False):
                data[task.user_id.id] = {
                    'name': task.user_id.name,
                    'email': task.user_id.email,
                    'image': task.user_id.image
                }

            user_data = data[task.user_id.id]
            if not user_data.get('project_data', False):
                user_data['project_data'] = {}

            user_project_data = user_data['project_data']
            if not user_project_data.get(task.project_id.id, False):
                user_project_data[task.project_id.id] = {
                    'project_name': task.project_id.name,
                    'open': 0,
                    'delay': 0,
                    'finish_this_month': 0,
                    'finish_this_week': 0,
                }

            user_project_detail = user_project_data[task.project_id.id]
            if task.stage_id.name.lower() == 'open':
                user_project_detail['open'] = user_project_detail['open'] + 1
            elif task.stage_id.name.lower() == 'delay':
                user_project_detail['delay'] = user_project_detail['delay'] + 1
            elif task.stage_id.name.lower() == 'done':
                _check_month_week(task, user_project_detail)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
