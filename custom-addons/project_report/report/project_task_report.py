# -*- coding: utf-8 -*-

from openerp import models, fields


class report_project_task(models.AbstractModel):
    _name = 'report.project_report.report_project_task'
    _inherit = 'report.abstract_report'
    _template = 'project_report.report_project_task'

    def render_html(self, cr, uid, ids, data=None, context=None):
        report_obj = self.pool['report']
        report = report_obj._get_report_from_name(cr, uid, 'project_report.report_project_task')
        project_task_obj = self.pool['project.task']
        all_tasks_ids = project_task_obj.search(cr, uid, [('project_id', '!=', False)], context=context)
        all_task_records = project_task_obj.browse(cr, uid, all_tasks_ids, context=context)

        data = {}
        self.pool['project.task.summary'].process_report_data(all_task_records, data)

        '''
        Construct list from dict object
        '''
        user_data_list = list(data.values())
        for user_data in user_data_list:
            user_data['project_data'] = list(user_data['project_data'].values())

        docargs = {
            'doc_ids': all_tasks_ids,
            'doc_model': report.model,
            'docs': user_data_list,
        }
        return report_obj.render(cr, uid, ids, 'project_report.report_project_task', docargs, context=context)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
